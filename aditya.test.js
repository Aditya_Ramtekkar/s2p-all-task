const sup = require("supertest");
var url = "http://109.106.255.69:2208/";

// beforeAll(() => {
//   console.log("before test cases");
// });

// beforeEach(() => {
//   console.log("start");
// });

// afterEach(() => {
//   console.log("end");
// });

// afterAll(() => {
//   console.log("after test cases");
// });

describe("User creation", () => {
  test.skip("POST user/signup -pass- user signup", async () => {
    const body = await sup(url).post("register").send({
      studentName: "Pifffkcxzramm",
      phone: "8756202490",
      email: "ppaapp4554@gmail.com",
      rollNo: "me4123",
      password: "1289456",
    });
    console.log("account creation", body);
    expect(body.statusCode).toBe(200);
  });
});

describe("User login", () => {
  test("POST user/Login -pass- user signup", async () => {
    const body = await sup("http://109.106.255.69:2208/student/login")
      .post("")
      .send({
        email: "adi@gmail.com",
        password: "adi123",
      });
    console.log("login ", body);
    expect(body.statusCode).toBe(200);
  });
});

describe("add contact", () => {
  test("POST user/contact added -pass- user signup", async () => {
    const body = await sup("http://109.106.255.69:2208/contact/addContact")
      .post("")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjE5LCJpYXQiOjE2NjIzODMwNzgsImV4cCI6MTY2MjM4NjY3OH0.IedZt7xteIrnOnukwRMGCPV7byH8HwD70xNpJqf7og4"
      )
      .send({
        name: "golu",
        phone: 8888895566,
        email: "golu@gmail.com",
        description: "Golu is an good actor\n",
      });
    console.log("contact creation", body);
    expect(body.statusCode).toBe(200);
  });
});

describe("Edit contact", () => {
  test("PUT user/Edit -pass- user signup", async () => {
    const body = await sup(
      "http://109.106.255.69:2208/contact/updateContactById/814"
    )
      .put("")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjE5LCJpYXQiOjE2NjIzODMwNzgsImV4cCI6MTY2MjM4NjY3OH0.IedZt7xteIrnOnukwRMGCPV7byH8HwD70xNpJqf7og4"
      )
      .send({
        id: 814,
        name: "diptanshu nasrae",
        phone: "8888895566",
        email: "golu@gmail.com",
        description: "Golu is an good actor",
      });
    console.log("login ", body);
    expect(body.statusCode).toBe(200);
  });
});
describe("Delete contact", () => {
  test("DELETE user/Delete -pass- user signup", async () => {
    const body = await sup(
      "http://109.106.255.69:2208/contact/deleteContactById/817"
    )
      .delete("")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjE5LCJpYXQiOjE2NjIzODMwNzgsImV4cCI6MTY2MjM4NjY3OH0.IedZt7xteIrnOnukwRMGCPV7byH8HwD70xNpJqf7og4"
      );
    expect(body.statusCode).toBe(200);
  });
});
