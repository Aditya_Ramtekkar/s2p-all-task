// console.log("Hello World");

// var a = 10;
// var b = 8;
// console.log(a * b);

// var stu1 = {
//   name: "Aditya",
//   roll: 126,
//   marks: 402,
//   college: "SBJIT",
//   address: {
//     street: "Golibar Chowk",
//     city: "NGP",
//     state: "MH",
//     country: "India",
//   },
//   contact: [1234, 5432],
// };

// console.log(stu1);
// console.log(stu1.address.city);

// function aditya(x) {
//   return x;
// }

// function family(b, c) {
//   return b * c;
// }
// console.log(aditya(2));
// console.log(family(2, aditya(5)));

// function animal(dogs, cat) {
//   console.log(dogs);
//   console.log(cat);
// }

// function dogs(labrador) {
//   dogs = {
//     breed: "labrador",
//     height: "2feeet",
//     name: "john",
//   };
//   return dogs;
// }

// function cat(persian) {
//   cat = {
//     breed: "persian",
//     height: "1feet",
//     name: "angelina",
//   };
//   return cat;
// }

// animal(dogs(), cat());

var student = [
  (stu1 = {
    name: "Aditya",
    roll: "126",
    college: "SBJIT",
    percentage: "82.88",
    address: {
      street: "Golibar Chowk",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [1234, 5432],
  }),
  (stu2 = {
    name: "Kapil",
    roll: "170",
    college: "SBJIT",
    percentage: "85.25",
    address: {
      street: "Sadar",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8234, 6988],
  }),
  (stu3 = {
    name: "Achal",
    roll: "105",
    college: "SBJIT",
    percentage: "75.54",
    address: {
      street: "Lalganj",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [6588],
  }),
  (stu4 = {
    name: "Simran",
    roll: "165",
    college: "SBJIT",
    percentage: "65.25",
    address: {
      street: "Golibar chowk",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [7888],
  }),
  (stu5 = {
    name: "Sonam",
    roll: "130",
    college: "VNIT",
    percentage: "84.25",
    address: {
      street: "LA",
      city: "Los Angeles",
      state: "Los Angeles",
      country: "United States",
    },
    contact: [8239],
  }),
  (stu6 = {
    name: "Lila",
    roll: "140",
    college: "VNIT",
    percentage: "79.25",
    address: {
      street: "Madanpur",
      city: "Akola",
      state: "MH",
      country: "India",
    },
    contact: [4488],
  }),
  (stu7 = {
    name: "Jayesh",
    roll: "147",
    college: "Priyadarshini",
    percentage: "45.25",
    address: {
      street: "Nandanvan",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8288],
  }),
  (stu8 = {
    name: "Soham",
    roll: "154",
    college: "Priyadarshini",
    percentage: "75.25",
    address: {
      street: "Lalpura",
      city: "Akola",
      state: "MH",
      country: "India",
    },
    contact: [8288],
  }),
  (stu9 = {
    name: "Adarsh",
    roll: "141",
    college: "SBJIT",
    percentage: "69.25",
    address: {
      street: "Wathodha",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [4555, 8845],
  }),
  (stu10 = {
    name: "Ayush",
    roll: "165",
    college: "SBJIT",
    percentage: "65.29",
    address: {
      street: "Friends colony",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8269],
  }),
  (stu11 = {
    name: "Komal",
    roll: "112",
    college: "SBJIT",
    percentage: "83.75",
    address: {
      street: "mankapur",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8888],
  }),
  (stu12 = {
    name: "Abhisekh",
    roll: "142",
    college: "SBJIT",
    percentage: "81.25",
    address: {
      street: "Wadi",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [3488],
  }),
  (stu13 = {
    name: "Nidhi",
    roll: "127",
    college: "SBJIT",
    percentage: "84.00",
    address: {
      street: "Wadi",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8238],
  }),
  (stu14 = {
    name: "Maitreya",
    roll: "115",
    college: "SBJIT",
    percentage: "74.25",
    address: {
      street: "Nandanwan",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8299],
  }),
  (stu15 = {
    name: "Sumedh",
    roll: "168",
    college: "SBJIT",
    percentage: "49.25",
    address: {
      street: "Tukdoji putla",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [8265],
  }),
  (stu16 = {
    name: "Diptanshu",
    roll: "133",
    college: "SBJIT",
    percentage: "48.95",
    address: {
      street: "Hudkeshwar",
      city: "NGP",
      state: "MH",
      country: "India",
    },
    contact: [1288],
  }),
  (stu17 = {
    name: "Sneha",
    roll: "120",
    college: "YCCE",
    percentage: "35.25",
    address: {
      street: "Madanpur",
      city: "Ahemdabad",
      state: "GJ",
      country: "India",
    },
    contact: [8998],
  }),
  (stu18 = {
    name: "Karan",
    roll: "144",
    college: "YCCE",
    percentage: "54.25",
    address: {
      street: "Manpur",
      city: "Ahemdabad",
      state: "GJ",
      country: "India",
    },
    contact: [8289],
  }),
  (stu19 = {
    name: "Vidya",
    roll: "125",
    college: "Raisoni",
    percentage: "55.25",
    address: {
      street: "Manipur",
      city: "Surat",
      state: "GJ",
      country: "India",
    },
    contact: [8234, 6988],
  }),
  (stu20 = {
    name: "Divya",
    roll: "121",
    college: "Raisoni",
    percentage: "79.25",
    address: {
      street: "Manipur",
      city: "Surat",
      state: "GJ",
      country: "India",
    },
    contact: [9888],
  }),
];

// if (student.city == "nagpur") {
//   // console.log(stud.dash);
//   console.log(student.name);
// console.log(stud.phoneno[i]);
// console.log(stud.branch[i]);
// console.log(stud.city[i]);
// console.log(stud.dash);
// }
// console.log(student.stu1.address);

// // console.log(student);

// var info = {
//   name: ["Divya", "Aditya", "Kapil", "Adarsh", "jhon", "Aarav"],
//   roll: ["121", "126", "127", "128", "165", "166"],
//   college: ["Raisoni", "SSBJIT", "SBJIT", "SBJIT", "Raisoni", "Raisoni"],
//   percentage: ["79.25", "88.12", "66.66", "45.66", "55.88", "76.01"],
//   address: {
//     city: ["Surat", "NGP", "NGP", "NGP", "Surat", "Surat"],
//     state: ["GJ", "MH", "MH", "MH", "GJ", "GJ"],
//     country: ["India", "India", "India", "India", "India", "India", "India"],
//   },
//   contact: [9888, 8999, 9000, 8888, 7899, 9111],
//   star: "********************************",
//   dash: "--------------------------------",
//   blank: " ",
// };

// console.log("SAME CITY NAGPUR : - ");
// console.log(info.star);
// for (i = 0; i < 20; i++) {
//   if (info.address.city[i] == "NGP") {
//     console.log(info.name[i]);
//     console.log(info.roll[i]);
//     console.log(info.college[i]);
//     console.log(info.percentage[i]);
//     console.log("Personal Information : - ");
//     console.log("City :", info.address.city[i]);
//     console.log("state :", info.address.state[i]);
//     console.log("country :", info.address.country[i]);
//     console.log(info.contact[i]);
//     console.log(info.dash);
//     console.log(info.star);
//     console.log(info.dash);
//   }
// }
// console.log(info.blank);
// console.log(info.blank);
// console.log(info.blank);
// console.log("Grades : - ");
// console.log(info.star);
// for (i = 0; i < 20; i++) {
//   if (info.percentage[i] >= 75) {
//     console.log("Category:- A ");
//     console.log(info.name[i]);
//     console.log(info.roll[i]);
//     console.log(info.college[i]);
//     console.log(info.percentage[i]);
//     console.log("Personal Information : - ");
//     console.log("City :", info.address.city[i]);
//     console.log("state :", info.address.state[i]);
//     console.log("country :", info.address.country[i]);
//     console.log(info.contact[i]);
//     console.log(info.dash);
//     console.log(info.star);
//     console.log(info.dash);
//   }
//   if (info.percentage[i] <= 75) {
//     console.log("Category:- B ");
//     console.log(info.name[i]);
//     console.log(info.roll[i]);
//     console.log(info.college[i]);
//     console.log(info.percentage[i]);
//     console.log("Personal Information : - ");
//     console.log("City :", info.address.city[i]);
//     console.log("state :", info.address.state[i]);
//     console.log("country :", info.address.country[i]);
//     console.log(info.contact[i]);
//     console.log(info.dash);
//     console.log(info.star);
//     console.log(info.dash);
//   }
// }

// // console.log(info);

{
  var pizzas = [
    (margrita_ = {
      price: "240 RS",
      base_size: "8 inch",
      crust: "Wheat thin crust",
      available_at: ["dominos", "Pizza hut", "Pizzenia"],
      type: "veg",
      discount: "20%",
      desciption:
        "Pizza margherita, as the Italians call it, is a simple pizza hailing from Naples. When done right, margherita pizza features a bubbly crust, crushed San Marzano tomato sauce, fresh mozzarella and basil, a drizzle of olive oil, and a sprinkle of salt",
    }),
    (chicken_tikka = {
      price: "300 RS",
      base_size: "12 inch",
      crust: "Classic hand tossed",
      available_at: ["dominos", "Pizza hut"],
      type: "non-veg",
      discount: "15%",
      desciption:
        "Grind tomatoes, garlic, green chillies and basil leaves in a mixer grinder. In a saucepan, heat butter and fry oregano on low heat. Add tomato puree mixture, chilli sauce and ketchup. Mix well on high heat till it boils.      ",
    }),
    (tandoori_paneer = {
      price: "280 RS",
      base_size: "12 inch",
      crust: "fresh pan pizza",
      available_at: ["dominos", "Pizza hut", "Pizzenia"],
      type: "veg",
      discount: "30%",
      desciption:
        "A flavorful Indian-inspired pizza topped with with tandoori mayo, paneer, capsicum and onion.",
    }),
    (capsicum = {
      price: "210 RS",
      base_size: "8 inch",
      crust: "fresh pan pizza",
      available_at: ["dominos"],
      type: "veg",
      discount: "20%",
      desciption:
        "Capsicum pizza is an Italian dish that is popular for its crispy crust, tomato sauce, and mild capsicum flavor. The name of this dish is derived from the ingredients – capsicum being the main ingredient used in this recipe.",
    }),
    (onion = {
      price: "200 RS",
      base_size: "8 inch",
      crust: "fresh pan pizza",
      available_at: ["dominos"],
      type: "veg",
      discount: "15%",
      desciption:
        "The best onion for pizza is white onion due to its strong but enjoyable flavor. White onions are a usual ingredient in many dishes, and pizza is no exception. Among all the types of onion, white ones combine best with other typical pizza toppings.",
    }),
    (cheesy_comfort = {
      price: "320 RS",
      base_size: "12 inch",
      crust: "cheese burst",
      available_at: ["Pizza hut"],
      type: "non-veg",
      discount: "15%",
      desciption:
        "Cheesy Creamy Pizza Comfort Topped With Onion, Green Capsicum, Red Capsicum & Sweet Corn",
    }),
    (mashroom = {
      price: "270 RS",
      base_size: "8 inch",
      crust: "new hand tossed",
      available_at: ["Pizza hut", "Pizzenia"],
      type: "veg",
      discount: "20%",
      desciption:
        "This mushroom pizza is a white pizza that’s covered with mozzarella cheese, goat cheese, and fresh herbs. It’s perfection!",
    }),
    (chicken_n_corn = {
      price: "310 RS",
      base_size: "8 inch",
      crust: "fresh pan pizza",
      available_at: ["dominos", "Pizzenia"],
      type: "non-veg",
      discount: "25%",
      desciption:
        "With store-bought dough and leftover roast chicken or a rotisserie chicken at hand, you can have this easy, cheesy enchilada-inspired pizza on the table in about an hour. ",
    }),
    (paneer_supreme = {
      price: "250 RS",
      base_size: "8 inch",
      crust: "cheesy burst",
      available_at: ["dominos", "Pizza hut", "Pizzenia"],
      type: "veg",
      discount: "20%",
      desciption: "Spiced Paneer, Herbed Onion & Green Capsicum, Red Paprika",
    }),
    (veg_exotica = {
      price: "220 RS",
      base_size: "8 inch",
      crust: "Wheat thin crust",
      available_at: ["dominos", "Pizza hut", "Pizzenia"],
      type: "veg",
      discount: "15%",
      desciption:
        "Delightful combination of garden fresh mushrooms, onions, olives and capsicums, sweetened with pineapple chunks and diced tomatoes.",
    }),
  ];
}

console.log("All Pizzas :- ", pizzas);
console.log(
  "Pizzas available at dominos :- ",
  pizzas.filter((x) => x.available_at == "dominos")
);

console.log(
  "Veg Pizzas :- ",
  pizzas.filter((x) => x.type == "veg")
);

console.log(
  " 8 inch Pizzas :- ",
  pizzas.filter((x) => x.base_size == "8 inch")
);

console.log(
  "Sorting :- ",
  pizzas.sort((a, b) => {
    if (a.price > b.price) return 1;
    else return -1;
  })
);
